﻿using UnityEngine;

[CreateAssetMenu(menuName="PG12/Ref/Float")]
public class RefFloat : ScriptableObject {

	public float value;
}


[CreateAssetMenu(menuName="PG12/Ref/Vector3")]
public class RefVector3 : ScriptableObject {

	public Vector3 position;
}