﻿using UnityEngine;

[CreateAssetMenu(menuName="PG12/MyFirstSO", fileName = "BartSimpson")]
public class SOTest : ScriptableObject
{
	[Range(0,10)]
	public int health;
	[Range(0,1)]
	public float speed;
}
