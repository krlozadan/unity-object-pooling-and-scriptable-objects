﻿using UnityEngine;

public class LifeSpan : MonoBehaviour {
	
	
	[SerializeField]
	private float lifeSpan = 3f;

	private void Awake()
	{
		Destroy(gameObject, lifeSpan);
	}

}
