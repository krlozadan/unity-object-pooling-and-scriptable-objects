﻿using System;
using UnityEngine;

public class PoolingTester : MonoBehaviour {

	[SerializeField]
	private GameObject notPooledProjectilePrefab;

	[SerializeField]
	private PoolableObject pooledProjectilePrefab;

	[SerializeField]
	private int length = 100;


	private void Start()
	{
		PoolManager.CreatePool(pooledProjectilePrefab, 4);
	}
	
	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Alpha1)) // Top left number button
		{
			for(int i = 0; i < length; i++)
			{
				Instantiate(notPooledProjectilePrefab, transform.position, Quaternion.identity);
			}
		}
		
		if(Input.GetKeyDown(KeyCode.Alpha2)) // Top left number button
		{
			var clone = PoolManager.GetObjectFromPool(pooledProjectilePrefab);
			clone.transform.position = transform.position + Vector3.right;
			clone.transform.rotation = Quaternion.identity; // Quaternion identity is always looking forward
			clone.gameObject.SetActive(true);
		}

		if(Input.GetKeyDown(KeyCode.Alpha0))
		{
            GC.Collect();
		}
	}

}
