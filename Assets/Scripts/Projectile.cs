﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour {

	[SerializeField]
	private float projectileSpeed = 30f;

	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	private void Enable()
	{
		rb.velocity = transform.forward * projectileSpeed;
	}
	
}
